//Q3. Organize a lista pelo panteão do deus.

function alfabetica(panteao){
    return function(a, b){
        if(a[panteao] < b[panteao]){
            return -1
        }else if(a[panteao] > b[panteao]){
            return 1
        }else{
            return 0 
        }
    }
}

gods.sort(alfabetica("pantheon"))
console.log(gods)
